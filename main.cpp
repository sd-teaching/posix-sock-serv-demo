#include <iostream>
#include <cstring>
#include <algorithm>

#include <unistd.h>

#include <thread>
#include <vector>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

bool needStop = false;
std::vector<int> clientSockets;

int clientThread(int newsockfd, int clientId) {
    int res;
    std::string result;
    do {
        int sz = 32;
        char *data = new char[sz+1] {0};

        res = recv(newsockfd, data, sz, 0);
        result+=data;

        if(result.find("\n\n")>=0) {
            std::cout << "Received: " << result << " from client " << clientId << std::endl;
            break;
        }
    } while(res != 0);

    std::this_thread::sleep_for(std::chrono::seconds(10));

    res = 0;
    std::string request("Hello from our super server!!\n\n");
    res = send(newsockfd, request.c_str(), request.length(), 0);
    if(res != request.length()) {
        std::cout << "Send error!" <<std::endl;
        return 0;
    }

    std::cout << "Sent " << res << " bytes" << std::endl;

    close(newsockfd);

    return 0;
}

int acceptThreadRoutine() {
    int sockfd;
    struct sockaddr_in serv_addr;
    struct sockaddr client_address;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        std::cout << "ERROR opening socket" << std::endl;
        return 0;
    }

    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port=htons(33124);
    serv_addr.sin_family=AF_INET;

    if(bind(sockfd, (sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        std::cout << "Bind error!" <<std::endl;
        return 0;
    }
    else {
        std::cout << "Address bound." << std::endl;
    }

    listen(sockfd, 5);

    socklen_t clilen = sizeof(client_address);
    while(!needStop) {
        int newsockfd = accept(sockfd, (struct sockaddr *) &client_address, &clilen);
        if (newsockfd < 0) {
            std::cout << "Accept error!" <<std::endl;
            close(sockfd);
            return -1;
        }
        clientSockets.push_back(newsockfd);
        std::cout << "Accept ok." << std::endl;

        std::thread client(clientThread, newsockfd, clientSockets.size());
        client.detach();
    }

    close(sockfd);

    return 0;
}

int main() {
    int sockfd, newsockfd, portno, n;

    std::cout << "Hello, Socket Server!" << std::endl;

    std::thread acceptThread(acceptThreadRoutine);

    std::cout << "Accept thread started" << std::endl;

    char c;
    std::cin>>c;

    needStop = true;
    acceptThread.join();

    return 0;
}